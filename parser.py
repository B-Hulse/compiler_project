from lexer import Lexer
from token import Token
from resolver import *
from compilerexceptions import *
from symbolTable import SymbolTable
import sys

class Parser:
    # Constructor to create the Lexer instance
    def __init__(self,filepath,resolver):
        self.lex = Lexer(filepath)
        self.sTables = []
        self.sTables.append(SymbolTable(filepath))
        self.tablePointer = 0
        self.currTable = self.sTables[self.tablePointer]
        self.resolver = resolver
        self.outFile = filepath.replace(".jack",".vm")
        self.cLabel = 0
        self.currLine = 0

        self.debug = True

    # Function to get the next token from the Lexer
    def getToken(self):
        next = self.lex.GetNextToken()
        self.currLine = next.lineNumber
        return next
    
    # Function to peek the next token from the Lexer
    def peekToken(self):
        return self.lex.PeekNextToken()

    # Function to raise an exception if a token is not found
    def badToken(self,token, expected = None):
        if token.type == "EOF":
            raise EOFError("unexpected EOF",self.currLine)
        else:
            outStr = "Bad token " + token.lexeme
            if expected:
                outStr += ", Expected: " + str(expected)
            raise BadTokenException(outStr,self.currLine)

    # Function to raise an exception if a variable is used before being declared
    def undeclaredVarError(self,name):
        raise BadIdentifierException(name.lexeme + " is used before being declared",self.currLine)

    # Debugging Function that returns a valid token
    def found(self,token):
        if self.debug:
            print(str(token.lineNumber) + ": found " + token.lexeme)

    # Start point of the parser
    def parse(self):
        # Open the output file
        self.out = open(self.outFile,"w")

        try:
            # Attempt to parse the source
            self.classDeclar()
        except CompilerException as e:
            # If an exception is thrown, display and exit (cleaning up the open file)
            print(e.msg)
            self.out.close()
            quit()

        # Clean up the open file
        self.out.close()

    # Check if the next token is a specified terminal
    def terminal(self, tType, lexeme=None):
        token = self.getToken()
        if token.checkValue(tType,lexeme):
            self.found(token)
        else:
            self.badToken(token,lexeme)
        return token

    # Functions for minipulating the SymbolTable array
    def addTable(self,name):
        self.sTables.append(SymbolTable(name))
        self.tablePointer += 1
        self.currTable = self.sTables[self.tablePointer]

    # Remove a symbol table when the scope of said table has been vacated by the compiler
    def removeTable(self):
        if self.debug:
            self.sTables[self.tablePointer].printTable()
            
        self.sTables.pop()
        self.tablePointer -= 1
        self.currTable = self.sTables[self.tablePointer]

    # Functions for checking the symbol table information
    def getVar(self, name):
        for table in self.sTables:
            v = table.getVar(name)
            if v:
                return v
        raise BadIdentifierException(name + " is used before being declared",self.currLine)

    # Returns a boolean, true a var exists int he current scope
    def isVar(self, name):
        for table in self.sTables:
            if table.getVar(name):
                return True
        return False

    # Returns a method if it is avilable in the current scope
    def getMethod(self, name):
        for table in self.sTables:
            m = table.getMethod(name)
            if m:
                return m
        raise BadIdentifierException(name + "() is used before being declared",self.currLine)


    def isMethod(self, name):
        for table in self.sTables:
            if table.getMethod(name):
                return True
        raise False

    # Return the type of a symbol in the symbol table, if not present raise exception
    def getVarType(self, name):
        for table in self.sTables:
            v = table.getVar(name)
            if v:
                return v[2]
        raise BadIdentifierException(name + " is used before being declared",self.currLine)

    # Return the type of a symbol in the symbol table, if not present raise exception
    def getMethodType(self, name):
        for table in self.sTables:
            m = table.getMethod(name)
            if m:
                return m[2]
        raise BadIdentifierException(name + "() is used before being declared",self.currLine)

    # Set the variable in the symbol table ot be assigned
    def setAssigned(self,name):
        for table in self.sTables:
            if table.getVar(name):
                table.setAssigned(name)
                return
        raise BadIdentifierException(name + " is used before being declared",self.currLine)

    # Check if a variable has been assigned, if not raise an exception
    def checkAssigned(self,name):
        for table in self.sTables:
            if table.getVar(name):
                if table.isAssigned(name):
                    return
                else:
                    raise UnassignedVarException(name + " is used before it has been assigned",self.currLine)
        raise BadIdentifierException(name + " is used before being declared",self.currLine)

    # Writes a line or lines of code to the output file
    def writeCode(self,strIn):
        # If a list of strings is given, run the function recursively on each item of the list
        # This allows printing multiple lines with one function call
        if isinstance(strIn,list):
            for str in strIn:
                self.writeCode(str)

            return

        # Code for single line goes here
        self.out.write(strIn + "\n")

    # Functions for the Productions
    # -----------------------------
    # In the comment production rules *x signifies x is a terminal

    # class -> *class *identifier *{ {memberDeclar}*}
    def classDeclar(self):
        # Check that the next token is a terminal with the type "keyword" and the lexeme "class"
        self.terminal("keyword","class")

        id = self.terminal("id")

        # Store a pointer to the resolver version of the class being parsed
        self.resolved = self.resolver.getClass(id.lexeme)

        self.terminal("symbol","{")

        #Initialise the SymbolTable Array
        self.addTable(id.lexeme)

        # While the next token is not a closed parenthesis
        while not self.peekToken().checkValue("symbol","}"):
            self.memberDeclar()
        
        self.terminal("symbol","}")

        self.removeTable()

    # memberDeclar -> classVarDeclar | subroutineDeclar
    def memberDeclar(self):
        # This functoin must look ahead to see which production to call
        t = self.peekToken()
        if t.checkValue("keyword",("static","field")):
            self.classVarDeclar()
        elif t.checkValue("keyword",("constructor","function","method")):
            self.subroutineDeclar()
        else:
            self.badToken(t,("static","field","constructor","function","method"))

    # classVarDeclar -> (*static|*field) type *identifier {*, *identifier}*;
    def classVarDeclar(self):
        # Parse and store the kind, type and id of the variable
        kind = self.terminal("keyword",("static","field"))

        # Get the type of the var
        type = self.typeDef()

        id = self.terminal("id")

        # Add the variable to the symbol table
        self.currTable.addVar(id.lexeme,kind.lexeme,type.lexeme)

        # While the next token is a ',' meaning multiple vars are declared in one line
        while self.peekToken().checkValue("symbol",","):
            self.terminal("symbol",",")
            id = self.terminal("id")
            self.currTable.addVar(id.lexeme,kind.lexeme,type.lexeme)
        
        self.terminal("symbol",";")

    # typeDef -> *int | *char | *boolean | *identifier
    def typeDef(self):
        # Check the next token but do not pass it
        t = self.peekToken()
        # If it is an id, this means the var is a custom type
        if t.checkValue("id"):
            self.terminal("id")
        else:
            self.terminal("keyword",("int","char","boolean"))
        return t

    # subroutineDeclar -> (*constructor|*function|*method)(type|*void)*identifier *([paramList]*) subroutineBody
    def subroutineDeclar(self):
        # Find the kind, id and type of the subroutine
        kind = self.terminal("keyword",("constructor","function","method"))

        type = self.peekToken()
        if type.checkValue("keyword","void"):
            self.terminal("keyword","void")
        else:
            self.typeDef()

        id = self.terminal("id")

        # Add the sub to the symbol table
        self.currTable.addMethod(id.lexeme,kind.lexeme,type.lexeme)

        self.terminal("symbol","(")

        self.addTable(id.lexeme)

        # Parse the parameters of the subroutine definition
        if not self.peekToken().checkValue("symbol",")"):
            self.paramList()

        self.terminal("symbol",")")

        # Get the ResolvedMember representing the method
        rMember = self.resolved.getMember(id.lexeme,"method")

        # Output to the vm file, declaring the subroutine
        self.writeCode("function " + self.resolved.id + "." + id.lexeme + " " + str(len(rMember.vars)))
        
        # If its a constructor, create the object in memory
        if (kind.checkValue("keyword","constructor")):
            fCount = len(self.resolved.fields)
            self.writeCode(["push constant " + str(fCount),"call Memory.alloc 1","pop pointer 0"])
        # If its a method set 'this' to the object it is running on
        elif (kind.checkValue("keyword","method")):
            self.writeCode(["push argument 0","pop pointer 0"])

        self.subroutineBody(type)
    
    # paramList -> type *identifier {*, type *identifier}
    def paramList(self):
        # Get the data type and id of the first parameter
        type = self.typeDef()
        id = self.terminal("id")

        # Add it to the symbol table
        # NOTE: The 'assigned' field is set to true because when the function is run, we can assume the arguments are given and thus assigned
        self.currTable.addVar(id.lexeme,"argument",type.lexeme,True)

        # Repeat for all further parameters
        while self.peekToken().checkValue("symbol",","):
            self.terminal("symbol",",")
            type = self.typeDef()
            id = self.terminal("id")

            self.currTable.addVar(id.lexeme,"argument",type.lexeme,True)

    # subroutineBody -> *{{statement}*}
    def subroutineBody(self,sType):
        self.terminal("symbol","{")

        # No ret is a default value if no return has been called in the function
        retType = "NORET"
        while not self.peekToken().checkValue("symbol","}"):
            # Statement will always return "NORET" unless the statement was a return statement
            retType = self.statement()
            # If the statement was a return, check it is the right type
            if not retType == "NORET":
                if not (sType.lexeme == retType or retType == "NULL"):
                    raise TypeMismatchException(retType + " was returned but " + sType.lexeme + " was expected",self.currLine)
        # If the last statement was not a return statement
        if retType == "NORET":
            raise UnreachableCodeException("Unreachable Code or no return statement detected",self.currLine)

        self.terminal("symbol","}")

        self.removeTable()

    # statement -> varDeclarStatement | letStatement | ifStatement | whileStatement | doStatement | returnStatement
    def statement(self):
        # Look ahead to check what the next statement is and look for said statement
        t = self.peekToken()
        if t.checkValue("keyword","var"):
            self.varDeclarStatement()
        elif t.checkValue("keyword","let"):
            self.letStatement()
        elif t.checkValue("keyword","if"):
            self.ifStatement()
        elif t.checkValue("keyword","while"):
            self.whileStatement()
        elif t.checkValue("keyword","do"):
            self.doStatement()
        elif t.checkValue("keyword","return"):
            return self.returnStatement()
        return "NORET"

    # varDeclarStatement -> *var type *identifier {*, *identifier}*;
    # This is similar to the classVarDeclar
    def varDeclarStatement(self):
        self.terminal("keyword","var")

        type = self.typeDef()
        id = self.terminal("id")

        self.currTable.addVar(id.lexeme,"var",type.lexeme)

        while not self.peekToken().checkValue("symbol",";"):
            self.terminal("symbol",",")
            id = self.terminal("id")

            self.currTable.addVar(id.lexeme,"var",type.lexeme)

        self.terminal("symbol",";")

    # letStatement -> *let *identifier [*[expression*]]*= expression *;
    def letStatement(self):
        self.terminal("keyword","let")
        id = self.terminal("id")

        # Check that the var being assigned exists and set it having been assigned
        self.isVar(id.lexeme)
        self.setAssigned(id.lexeme)

        # Find the var type
        lhsType = self.getVarType(id.lexeme)

        # Boolean to state whether the var being assigned is an item in an array
        arr = False

        # If it is an item in an array
        if self.peekToken().checkValue("symbol","["):
            self.terminal("symbol","[")
            # Find the index and check that it is an int
            indexType = self.expression()
            if not indexType == "int":
                raise TypeMismatchException("Cannot index an array with a non-integer value",self.currLine)
            self.terminal("symbol","]")
            # Null is used as a placeholder to allow all types to be assigned to array index
            lhsType = "null"

            # Get the var details from the symbol table
            v = self.getVar(id.lexeme)

            # Set the that pointer to point to the target array index
            segment = v[1]
            if segment == "var":
                segment = "local"
            elif segment == "field":
                segment = "this"
            self.writeCode(["push " + segment + " " + str(v[3]),"add","pop pointer 1"])
            arr = True

        self.terminal("symbol","=")
        rhsType = self.expression()
        self.terminal("symbol",";")

        s = self.getVar(id.lexeme)
        # If it is an array index, pop the top of the stack to the that segment
        if arr:
            self.writeCode("pop that 0")
        # Else generate the VM code to pop to the variable
        else:
            segment = str(s[1])
            if segment == "var":
                segment = "local"
            elif segment == "field":
                segment = "this"
            self.writeCode("pop " + segment + " " + str(s[3]))

        # If the assignment is incompatible types
        if not (lhsType == rhsType or lhsType == "null" or rhsType == "null"):
            raise TypeMismatchException("Cannot assign type " + str(rhsType) + " to variable with type " + str(lhsType),self.currLine)

    # ifStatement -> *if *( expression *) *{{statement}*}[*else*{{statement}*}]
    def ifStatement(self):
        self.terminal("keyword","if")
        self.terminal("symbol","(")
        self.expression()
        self.terminal("symbol",")")

        # Get a unique id for the labels related to this if
        ifID = self.cLabel
        self.cLabel += 1

        # If the expression is not true, jump to the else label
        self.writeCode(["neg","if-goto else" + str(ifID)])

        self.terminal("symbol","{")

        self.addTable("if statement")

        # While again checking that the last statement is a return, compile the contents of the if body
        returned = False
        while not self.peekToken().checkValue("symbol","}"):
            ret = self.statement()
            if ret == "NORET":
                returned = True
            elif returned:
                raise UnreachableCodeException("Unreachable Code detected",self.currLine)

        self.terminal("symbol","}")

        self.removeTable()

        # Create the else label and jump to the end
        self.writeCode(["goto end"+str(ifID),"label else" + str(ifID)])

        # The else body is the same as the if body
        if self.peekToken().checkValue("keyword","else"):
            self.terminal("keyword","else")
            self.terminal("symbol","{")

            self.addTable("else statement")

            returned = False
            while not self.peekToken().checkValue("symbol","}"):
                ret = self.statement()
                if ret == "NORET":
                    returned = True
                elif returned:
                    raise UnreachableCodeException("Unreachable Code detected",self.currLine)


            self.terminal("symbol","}")

            self.removeTable()
        
        # create the end label
        self.writeCode("label end" + str(ifID))

    # whileStatement -> *while *(expression*)*{{statement}*}
    def whileStatement(self):
        # A lof of this code is simlar to the if statement
        self.terminal("keyword","while")

        whileID = self.cLabel
        self.cLabel += 1
        # Create a label for the start of the loop
        self.writeCode("label loop" + str(whileID))

        self.terminal("symbol","(")
        self.expression()
        self.terminal("symbol",")")
                
        # Evaluate the expression and jump to escape the loop if false
        self.writeCode(["neg","if-goto end" + str(whileID)])

        self.terminal("symbol","{")

        self.addTable("while loop")

        # Finding the contents of the loop body (same as if)
        returned = False
        while not self.peekToken().checkValue("symbol","}"):
            ret = self.statement()
            if ret == "NORET":
                returned = True
            elif returned:
                raise UnreachableCodeException("Unreachable Code detected",self.currLine)

        self.terminal("symbol","}")

        self.removeTable()

        # Jump back to the start of the loop, creating an end label on the next line
        self.writeCode(["goto loop" + str(whileID),"label end" + str(whileID)])

    # doStatement -> *do subroutineCall*;
    def doStatement(self):
        self.terminal("keyword","do")
        self.subroutineCall()
        self.terminal("symbol",";")

    # subroutineCall -> *identifier [*.*identifier]*([expressionList]*)
    def subroutineCall(self):
        id = self.terminal("id")
        # If 'id' is followed by a '.', it must be a class or object
        # Use the methods defined for parsing class calls
        if self.peekToken().checkValue("symbol","."):
            lhsType = self.outOfClassCall(id,"method")
        else:
            lhsType = self.inClassCall(id,"method")
        
        # Remove the top
        self.writeCode("pop temp 0")

    # expressionList -> expression {*, expression}
    def expressionList(self):
        # Find the first expression and add it to a list
        expType = self.expression()
        expTypes = [expType]
        # Repeat for each following expression
        while self.peekToken().checkValue("symbol",","):
            self.terminal("symbol",",")
            expType = self.expression()
            expTypes.append(expType)

        return expTypes

    # returnStatement -> *return [expression]*;
    def returnStatement(self):
        self.terminal("keyword","return")
        # As the return expression is optional, check if it is present
        if not self.peekToken().checkValue("symbol",";"):
            retType = self.expression()
            self.terminal("symbol",";")
            self.writeCode("return")
            return retType
        self.terminal("symbol",";")

        # If no expression is present, push 0 to the stack to signify a void return
        self.writeCode(["push constant 0","return"])
        return "void"

    # Expression productions
    # Each of these will evaluate the left and right seperately, then check that the types are compatible

    #expression -> relationalExpression {(*&|*|) relationalExpression}
    def expression(self):
        lhsType = self.relationalExpression()
        while self.peekToken().checkValue("symbol",("&","|")):
            t = self.terminal("symbol",("&","|"))
            rhsType = self.relationalExpression()
            # If types are not compatible
            if not (lhsType == rhsType or lhsType == "null" or rhsType == "null"):
                raise TypeMismatchException("Cannot compare type " + rhsType + " with type " + lhsType)
            # Do the operation on the top two items of the stack
            if t.checkValue("symbol","&"):
                self.writeCode("and")
            elif t.checkValue("symbol","|"):
                self.writeCode("or")
        # Return the type of the expression
        return lhsType
    
    # relationalExpression -> ArithmeticExpression {(*=|*>|*<|)ArithmeticExpression}
    def relationalExpression(self):
        lhsType = self.ArithmeticExpression()
        while self.peekToken().checkValue("symbol",("=",">","<")):
            t = self.terminal("symbol",("=",">","<"))
            rhsType = self.ArithmeticExpression()
            if t.checkValue("symbol","="):
                self.writeCode("eq")
            elif t.checkValue("symbol",">"):
                self.writeCode("gt")
            elif t.checkValue("symbol","<"):
                self.writeCode("lt")
            # If the symbols were present, then the expression was a comparison and shoulf return a boolean
            return "boolean"
        return lhsType

    # ArithmeticExpression -> term {(*+|*-)term}
    def ArithmeticExpression(self):
        lhsType = self.term()
        while self.peekToken().checkValue("symbol",("+","-")):
            t = self.terminal("symbol",("+","-"))
            rhsType = self.term()
            if not (lhsType == rhsType or lhsType == "null" or rhsType == "null"):
                raise TypeMismatchException("Cannot use type " + rhsType + " with type " + lhsType)
            if t.checkValue("symbol","+"):
                self.writeCode("add")
            elif t.checkValue("symbol","-"):
                self.writeCode("sub ")
        return lhsType

    # term factor {(**|*/)factor}
    def term(self):
        lhsType = self.factor()
        while self.peekToken().checkValue("symbol",("*","/")):
            t = self.terminal("symbol",("*","/"))
            rhsType = self.factor()
            if not (lhsType == rhsType or lhsType == "null" or rhsType == "null"):
                raise TypeMismatchException("Cannot use type " + rhsType + " with type " + lhsType)
            if t.checkValue("symbol","*"):
                self.writeCode("call Math.multiply 2")
            elif t.checkValue("symbol","/"):
                self.writeCode("call Math.divide 2")

        
        return lhsType

    #factor -> [*-|*~]operand
    def factor(self):
        t = self.peekToken()
        if t.checkValue("symbol",("-","~")):
            self.terminal("symbol",("-","~"))
        lhsType = self.operand()

        if t.checkValue("symbol",("-","~")):
            self.writeCode("neg")

        return lhsType

    # operand -> *integerConstant | *identifier [*.*identifier] [*{expression*}|*(expressionList*)]|*(expression*)|*stringLiteral|*true|*false|*null|*this
    # For calls that are to an object or class member or to a function or method, I have seperated them out into two functions below
    def operand(self):
        # If lhsType == "NO TYPE FOUND" at the end then the operand was unsuccesfully parsed
        lhsType = "NO TYPE FOUND"
        t = self.peekToken()

        # If the operand is a number
        if t.checkValue("number"):
            self.terminal("number")
            lhsType = "int"

            self.writeCode("push constant " + t.lexeme)
        # If the operand is an expressions
        elif t.checkValue("symbol","("):
            self.terminal("symbol","(")
            lhsType = self.expression()
            self.terminal("symbol",")")
        # If the operand is a string literal
        elif t.checkValue("string"):
            self.terminal("string")
            lhsType = "String"
        # If the operand is a boolean literal
        elif t.checkValue("keyword", ("true","false")):
            self.terminal("keyword",("true","false"))
            lhsType = "boolean"

            if t.checkValue("keyword","false"):
                self.writeCode("push constant 0")
            elif t.checkValue("keyword","true"):
                self.writeCode(["push constant 1","neg"])
        # If the operand is the 'this' keyword
        elif t.checkValue("keyword", "this"):
            self.terminal("keyword","this")
            lhsType = self.resolved.id
        # Otherwise the operand must be null
        # If the operand is a variable or function
        elif t.checkValue("id"):
            id = self.terminal("id")
            # If 'id' is followed by a '.', it must be a class or object
            if self.peekToken().checkValue("symbol","."):
                lhsType = self.outOfClassCall(id)
            else:
                lhsType = self.inClassCall(id)
        else:
            self.terminal("keyword","null")
            lhsType = "null"

            self.writeCode("push constant 0")

        return lhsType
    
    # If the operand is a call to a member of a class
    def outOfClassCall(self,id,method = False):
        lhsType = "NO TYPE FOUND"
        classID = id.lexeme
        # If its an object declared in the current scope
        if self.isVar(id.lexeme):
            # Find the type of the object
            classID = self.getVarType(id.lexeme)

        
        self.terminal("symbol",".")
        memberID = self.terminal("id").lexeme

        # Function call
        if self.peekToken().checkValue("symbol","("):
            # Get the method data from the resolver
            cMethod = self.resolver.getClass(classID).getMember(memberID,"method")
            lhsType = cMethod.type

            # If the calling class is the same as the current class, push the current object onto the stack
            if classID == id.lexeme:
                self.writeCode("push argument 0")

            self.terminal("symbol","(")
            # If parameters are given
            if not self.peekToken().checkValue("symbol",")"):
                argTypes = self.expressionList()
                # Check that the parameters are compatible with the method call
                cMethod.checkArgs(argTypes)

            self.terminal("symbol",")")

            # Code Gen

            aCount = len(cMethod.args)
            # If it is a method, add one to the argument count
            if classID == id.lexeme:
                aCount += 1
            self.writeCode("call "+classID + "." + memberID + " " + str(aCount))

        # Variable reference
        else:
            # If the call is only looking for a method, raise an exception
            if method:
                raise BadIdentifierException(id.lexeme + " is not a valid function name",self.currLine)
            
            # This code is very similar to the function call unless commented
            
            cVar = self.resolver.getClass(classID).getVar(memberID)
            lhsType = cVar.type        
        
            s = self.getVar(id.lexeme)
            # find the offset into the field segment of the class being called on
            varIndex = self.resolver.getClass(classID).findField(cVar)
            # Write the VM commands to access the variable being addressed
            segment = str(s[1])
            if segment == "var":
                segment = "local"
            elif segment == "field":
                segment = "this"
            self.writeCode(["push " + segment + " " + str(s[3]),"pop pointer 1","push that " + str(varIndex)])

            # If the operand is addressing an array index
            if self.peekToken().checkValue("symbol","["):
                self.terminal("symbol","[")
                # Check the index is valid
                indexType = self.expression()
                if not indexType == "int":
                    raise TypeMismatchException("Cannot index an array with a non-integer value",self.currLine)
                self.terminal("symbol","]")
                lhsType = "null"

                # Use the item on top of the stack as a pointer with the offset and set the index of the array to the that segment
                self.writeCode(["add","pop pointer 1","push that 0"])

        return lhsType

    # This is called if a subroutine or variable are accessed
    def inClassCall(self,id,method = False):
        lhsType = "NO TYPE FOUND"
        # Function call
        if self.peekToken().checkValue("symbol","("):
            # Get the subroutine information from the resolver
            m = self.resolved.getMember(id.lexeme,"method")

            # If it is a method push this onto the stack
            if m.kind == "method":
                self.writeCode("push argument 0")

            lhsType = m.type
            self.terminal("symbol","(")
            # If parameters are given check like was done for outside class calls
            if not self.peekToken().checkValue("symbol",")"):
                argTypes = self.expressionList()
                m.checkArgs(argTypes)
            self.terminal("symbol",")")

            # Code Gen to call the subroutine
            aCount = len(m.args)
            if m.kind == "method":
                aCount += 1
            self.writeCode("call "+ self.resolved.id + "." + id.lexeme + " " + str(aCount))
        # Variable reference
        else:
            # This code is almost identical to the code for an outside class call, with the exception of not needing information about the class to which the variable belongs

            if method:
                raise BadIdentifierException(id.lexeme + " is not a valid function name",self.currLine)
            self.isVar(id.lexeme)
            lhsType = self.getVarType(id.lexeme) 

            self.checkAssigned(id.lexeme)

            s = self.getVar(id.lexeme)
            segment = str(s[1])
            if segment == "var":
                segment = "local"
            elif segment == "field":
                segment = "this"
            self.writeCode("push " + segment + " " + str(s[3]))  

            if self.peekToken().checkValue("symbol","["):
                self.terminal("symbol","[")
                indexType = self.expression()
                if not indexType == "int":
                    raise TypeMismatchException("Cannot index an array with a non-integer value",self.currLine)
                self.terminal("symbol","]")
                # Null is used as a placeholder as 
                lhsType = "null"

                self.writeCode(["add","pop pointer 1","push that 0"])
        return lhsType