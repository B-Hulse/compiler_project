from compilerexceptions import *
from lexer import Lexer
from token import Token

# Resolver Stores all of the compiled classes and their members
class Resolver:
    def __init__(self):
        # Keeps a list of all the classes in a given compilation
        self.classes = []
        # Keeps a reference to the last added class to the list
        self.currClass = None
        self.currLine = 0

    # Add a class to the resolver
    def addClass(self,id):
        if self.isClass(id):
            raise BadClassException(id + "has already been declared as a class",self.currLine)

        self.classes.append(ResolvedClass(id))
        self.currClass = self.getClass(id)

    # Return true if the class with the id given has been resolved
    def isClass(self,id):
        for c in self.classes:
            if c.id == id:
                return True
        return False

    # Return the class with the id given
    def getClass(self,id):
        if not self.isClass(id):
            raise BadClassException(id + " is not a resolved class",self.currLine)

        for c in self.classes:
            if c.id == id:
                return c

    # Function to begin resolving a file at the given filepath
    def resolveFile(self,filepath):
        self.cFile = filepath
        self.lex = Lexer(filepath)
        try:
            self.classDeclar()
        except CompilerException as e:
            print(e.msg)
            quit()

    # ------ Functions responsible for resolving classes ------
    # Note: Some of this code will be similar to the parser code, however it is simplified and so I do not use the same code

    # Function to get the next token from the Lexer
    def getToken(self):
        next = self.lex.GetNextToken()
        self.currLine = next.lineNumber
        return next
    
    # Function to peek the next token from the Lexer
    def peekToken(self):
        return self.lex.PeekNextToken()

    # Check if the next token is a specified terminal
    def terminal(self, tType, lexeme=None):
        token = self.getToken()
        if token.checkValue(tType,lexeme):
            pass
        else:
            self.badToken(token,lexeme)
        return token

    # Function to raise an exception if a token is not found
    def badToken(self,token, expected = None):
        if token.type == "EOF":
            raise EOFError("Unexpected EOF",self.currLine)
        else:
            outStr = "at " + token.lexeme
            if expected:
                outStr += ", Expected: " + str(expected)
            raise BadTokenException(outStr,self.currLine)

    # Function to find the class definition
    def classDeclar(self):
        # Check that the next token is a terminal with the type "keyword" and the lexeme "class"
        self.terminal("keyword","class")

        id = self.terminal("id")

        # Add the class to the resolver
        self.addClass(id.lexeme)

        self.terminal("symbol","{")

        # While the next token is not a closed parenthesis find the members of the class
        while not self.peekToken().checkValue("symbol","}"):
            self.memberDeclar()
        
        self.terminal("symbol","}")

    # Function to find the members of a class
    def memberDeclar(self):
        # This function must look ahead to see which production to call and call the apropriate member kind
        t = self.peekToken()
        if t.checkValue("keyword",("static","field")):
            self.classVarDeclar()
        elif t.checkValue("keyword",("constructor","function","method")):
            self.subroutineDeclar()
        else:
            self.badToken(t,("static","field","constructor","function","method"))

    # Find the class var
    def classVarDeclar(self):
        # Store the kind, id and type of the var
        kind = self.terminal("keyword",("static","field"))

        type = self.typeDef()

        id = self.terminal("id")

        # Add the var to the class in the resolver
        self.currClass.addVar(id.lexeme,kind.lexeme,type.lexeme)

        # While the next token is a ',' do so for all following vars
        while self.peekToken().checkValue("symbol",","):
            self.terminal("symbol",",")
            id = self.terminal("id")
            self.currClass.addVar(id.lexeme,kind.lexeme,type.lexeme)
        
        self.terminal("symbol",";")

    # Identical to the parser typeDef()
    def typeDef(self):
        t = self.peekToken()
        if t.checkValue("id"):
            self.terminal("id")
        else:
            self.terminal("keyword",("int","char","boolean"))
        return t

    # Stripped down subroutine Declaration checker
    def subroutineDeclar(self):
        # Find the subroutine information
        kind = self.terminal("keyword",("constructor","function","method"))

        type = self.peekToken()
        if type.checkValue("keyword","void"):
            self.terminal("keyword","void")
        else:
            self.typeDef()

        methodID = self.terminal("id")
        
        self.terminal("symbol","(")

        # Find a list of types for the args of the subroutine
        args = []
        if not self.peekToken().checkValue("symbol",")"):
            args = self.paramList()

        self.terminal("symbol",")")        
        
        # This keeps track of the 'depth' and finds every '{','}' pair until the final '}' of that subroutine
        # It essentially skim reads through the subroutine body, ignoring its contents until it reaches the end
        self.terminal("symbol","{")
        scopeLevel = 0
        vars = []

        # Until the closing brace of the subroutine is found, look for variables in the subroutine body
        while not self.peekToken().checkValue("symbol","}") or scopeLevel > 0:
            t = self.peekToken();
            if t.checkValue("symbol","{"):
                self.terminal("symbol","{")
                scopeLevel += 1
            elif t.checkValue("symbol","}"):
                self.terminal("symbol","}")
                scopeLevel -= 1
            elif t.checkValue("keyword","var"):
                # Append the loval variable list of that subroutine with the variable(s) found
                self.terminal("keyword","var")
                self.typeDef()
                id = self.terminal("id")
                vars.append(id.lexeme)

                while not self.peekToken().checkValue("symbol",";"):
                    self.terminal("symbol",",")
                    id = self.terminal("id")
                    vars.append(id.lexeme)
            else:
                self.getToken()

        self.terminal("symbol","}")

        # Add the subroutine to the class being resolved
        self.currClass.addMethod(methodID.lexeme,kind.lexeme,type.lexeme,vars,args)
    
    def paramList(self):
        type = self.typeDef()
        self.terminal("id")

        args = [type.lexeme]

        while self.peekToken().checkValue("symbol",","):
            self.terminal("symbol",",")
            type = self.typeDef()
            self.terminal("id")
            args.append(type.lexeme)
        
        return args
                
# Resolved class stores the data of a class
class ResolvedClass:
    def __init__(self,id):
        self.id = id
        # List of member objects
        self.statics = []
        self.fields = []
        self.methods = []

    # Add a variable to the class
    def addVar(self,id,kind,type):
        if self.isMember(id,kind):
            return False
        if kind == "static":
            self.statics.append(ResolvedMember(id,kind,type))
        else:
            self.fields.append(ResolvedMember(id,kind,type))

    # Add a subroutine to the class
    def addMethod(self,id,kind,type,vars,args):
        if self.isMember(id,kind):
            return False
        self.methods.append(ResolvedMember(id,kind,type,vars,args))

    # Return true if a member by the name 'd' and with the kind 'kind' exists in the class
    def isMember(self,id,kind):
        if kind == "method":
            for m in self.methods:
                if m.id == id:
                    return True
        elif kind == "static":
            for s in self.statics:
                if s.id == id:
                    return True
        else:
            for f in self.fields:
                if f.id == id:
                    return True

        return False

    # Same as above but return the member
    def getMember(self,id,kind):        
        if kind == "method":
            for m in self.methods:
                if m.id == id:
                    return m
        elif kind == "static":
            for s in self.statics:
                if s.id == id:
                    return s
        else:
            for f in self.fields:
                if f.id == id:
                    return f

        return False

    # Find the offset of the field variable 'id' in the class, return false if not present
    def findField(self,id):
        if self.isMember(id,"field"):
            for f in self.fields:
                if f.id == id:
                    return self.fields.index(f)
        else:
            return False

# Class to represent a class member
class ResolvedMember:
    def __init__(self,id,kind,type,vars=[],args=[]):
        # Identifier of member
        self.id = id
        # data type of the member
        self.type = type
        # arguments stored as list of types
        self.args = []
        # kind: method/var
        self.kind = kind
        # how many local variables the method has
        self.vars = []

        # Add every item from args to self.args and from vars to self.vars
        for a in args:
            self.args.append(a);

        for v in vars:
            self.vars.append(v)

    # Check that the args given as an argument are compatible with the args of the method
    def checkArgs(self,args):
        for i in range(0,len(self.args)):
            if not (self.args[i] == args[i] or args[i] == "null"):
                return False
