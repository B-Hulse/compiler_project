from resolver import Resolver
from parser import Parser
from compilerexceptions import BadClassException, BadMemberException
import sys
import os

class Compiler:
    def __init__(self):
        self.resolver = Resolver()
        # If a command line argument was not given or too many were given
        if not len(sys.argv) == 2:
            print("invalid command line argument")
            exit()

        self.files = []

        # Get the first command line argument
        input = sys.argv[1]
        # If it is a filepath to a jack file, add that file to the files list
        if input.endswith(".jack"):
            self.files.append(input)
        # If it a directory, add every jack file in the directory to the files list
        elif input.endswith("/"):
            for file in os.listdir(input):
                if file.endswith(".jack"):
                    self.files.append(input + file)
        # If it is neither stop the program as the cla are invalid
        else:
            print("invalid command line argument")
            exit()

        # Compile the program
        self.compile()

    # Function to compile the program
    def compile(self):
        
        # Resolve the standard library and the sourcce files
        self.resolveStd();
        for f in self.files:
            print("Resolving: " + f)
            self.resolver.resolveFile(f)

        # Parse the source files
        for f in self.files:
            p = Parser(f,self.resolver)
            p.parse()


    # Function to add the classes from the standard library to the resolver
    def resolveStd(self):
        self.resolver.resolveFile("JACK_Std/Array.jack")
        self.resolver.resolveFile("JACK_Std/Keyboard.jack")
        self.resolver.resolveFile("JACK_Std/Math.jack")
        self.resolver.resolveFile("JACK_Std/Memory.jack")
        self.resolver.resolveFile("JACK_Std/Output.jack")
        self.resolver.resolveFile("JACK_Std/Screen.jack")
        self.resolver.resolveFile("JACK_Std/String.jack")
        self.resolver.resolveFile("JACK_Std/Sys.jack")

c = Compiler()