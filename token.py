class Token:
    def __init__(self):
        self.lexeme = ""
        self.type = None
        self.lineNumber = None

    # Function that checks if a token is of a type and its lexime is target or in the tuple target if multiple values are passed
    def checkValue(self, type, target = None):
        if self.type == type:
            if isinstance(target,tuple):
                if self.lexeme in target:
                    return True
            elif target == None or self.lexeme == target:
                return True
        return False

    def EOFToken(lineNumber):
        t = Token()
        t.type = "EOF"
        t.lineNumber = lineNumber
        t.lexeme = ""
        return t