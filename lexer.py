from token import Token 
from compilerexceptions import *
class Lexer:
    # Array of all the keywords in the JACK language
    keywords = {"class", "constructor","method","function","int","boolean","char","void","var","static","field","let","do","if","else","while","return","true","false","null","this"}
    # Array of all the symbols in the JACK language
    symbols = {"(",")","[","]","{","}",",",";","=",".","+","-","*","/","&","|","~","<",">"}

    # Constructor Function that initialises the lexer
    # filePath: is the input source for the lexer
    def __init__(self, filePath):
        try:
            sourceFile = open(filePath,"r")
            self.sourceText = sourceFile.read()
            self.currLine = 1
            self.currChar = 0
            self.textlen = len(self.sourceText)
            # A 'peeked' token is the last token to be peeked at, meaning it is returned but not consumed
            self.peeked = False
        except IOError:
            print("Error opening file")
            quit()

    # Increments the currChar pointer and returns the char at that point
    def NextChar(self):
        self.currChar += 1
        # If the currChar pointer has reached the EOF
        if self.currChar == self.textlen:
            return False

        return self.CurrChar()

    # Returns the string starting at the pointer currChar with length count
    def CurrChar(self,count=1):
        # Find the char at currChar
        outString = self.sourceText[self.currChar]
        # if more than one char is needed, append the output string with the extra letters
        if count > 1:
            for i in range(1,count):
                # If the EOF is reached
                if self.currChar + i == self.textlen:
                    break
                outString += self.sourceText[self.currChar + i]
        return outString

    # Removes whitespace and increments currChar to point to the first non-whitespace character
    def removeWS(self):
        c = self.CurrChar()
        while c.isspace():
            # If c is a new line then increment the currLine pointer
            if (c == '\n'):
                self.currLine += 1
                
            c = self.NextChar()
            # If NextChar returns False then the EOF has been reached
            if (not c):
                return False
        return True

    # Removes whitespace and increments currChar to point to the first non-whitespace character
    def removeComment(self):
        if self.CurrChar(2) == "//":
            c = self.CurrChar()
            # Loop until new line
            while not c == "\n":
                c = self.NextChar()
                # If NextChar returns False then the EOF has been reached
                if (not c):
                    return False
        if self.CurrChar(2) == "/*":
            c = self.CurrChar(2)
            # Loop until new line
            while not c == "*/":
                c = self.NextChar()
                if c == "\n":
                    self.currLine += 1
                if c:
                    c = self.CurrChar(2)
                else:
                    # If NextChar returns False then the EOF has been reached
                    if (not c):
                        raise EOFException("Unexpected EOF (No matching */)",self.currLine)
            self.currChar += 2
        return True
    
    # Each of these next 4 functions returns the lexeme for each of the token types
    def GetIDKeyword(self):
        c = self.CurrChar()
        outStr = c
        c = self.NextChar()
        while c and (c.isalpha() or c == '_' or c.isdigit()):
            outStr += c
            c = self.NextChar()

        return outStr
        
    def GetNumber(self):
        c = self.CurrChar()
        outStr = c
        c = self.NextChar()
        while c and (c.isdigit()):
            outStr += c
            c = self.NextChar()

        return outStr    

    def GetString(self):
        c = self.NextChar()
        outStr = c
        c = self.NextChar()
        while c and not (c == "\""):
            outStr += c
            c = self.NextChar()
            if (not c):
                raise EOFException("Unexpected EOF (No matching \")",self.currLine)
            if (c == "\n"):
                raise UnexpectedSymbolException("Unexpected new line (No matching \")",self.currLine)
        self.currChar += 1

        return outStr

    def GetSymbol(self):
        c = self.CurrChar()
        outStr = c
        self.currChar += 1
        return outStr

    # Function to get the next token from the source text
    # Returns:
    # The next token
    # False (If the EOF is reached)
    # -1 (If an invalid symbol is detected, THIS SHOULD CEASE COMPILER OPERATION)
    def GetToken(self):

        # Check if EOF is reached
        if self.currChar == self.textlen:
            return Token.EOFToken(self.currLine)


        # If the current character is a whitespace
        if (self.CurrChar().isspace()):
            # If removeWS returned False then it reached EOF
            if not self.removeWS():            
                return Token.EOFToken(self.currLine)


        # If the line begins with a /
        if self.CurrChar(2) == "//" or self.CurrChar(2) == "/*":
            # Remove the comment
            ret = self.removeComment()
            # If EOF is reached
            if not ret or ret == -1:
                return Token.EOFToken(self.currLine)
            return self.GetToken()

        # Declare the token to be returned
        outToken = Token() 

        c = self.CurrChar()
        
        # KEYWORD or IDENTIFIER
        if c.isalpha() or c == '_':
            # Get the lexeme
            outToken.lexeme = self.GetIDKeyword()
            # Set the token type
            if outToken.lexeme in self.keywords:
                outToken.type = "keyword"
            else:
                outToken.type = "id"
            # Set the token line number
            outToken.lineNumber = self.currLine
        # NUMBER
        elif c.isdigit():
            outToken.lexeme = self.GetNumber()
            outToken.type = "number"
            outToken.lineNumber = self.currLine
        # STRING LITERAL
        elif c == "\"":
            outToken.lexeme = self.GetString()
            outToken.type = "string"  
            outToken.lineNumber = self.currLine
        # SYMBOL
        elif c in self.symbols:
            outToken.lexeme = self.GetSymbol()
            outToken.type = "symbol"
            outToken.lineNumber = self.currLine
        # ANYTHING ELSE - Unrecognised symbols
        else:
            # Print error message and raise exception
            raise UnexpectedSymbolException("Symbol " + c + " not recognised",self.currLine)

        return outToken

    # Function that returns the next token in the source and absorbs it
    def GetNextToken(self):
        #If Peek has been called since last get
        if self.peeked:
            #Return peeked Token and set the value to false
            p = self.peeked
            self.peeked = False
            return p
        else:
            try:
                return self.GetToken()
            except (EOFException, UnexpectedSymbolException, BadTokenException) as e:
                print(e.msg)
                quit()

    # Function that returns the next token in the source without absorbing it
    def PeekNextToken(self):
        # If a token has not already been peeked, set the next token to be peeked
        if not self.peeked:
            try:
                self.peeked = self.GetToken()
            except (EOFException, UnexpectedSymbolException, BadTokenException) as identifier:
                print(e.msg)
                quit()
        return self.peeked
