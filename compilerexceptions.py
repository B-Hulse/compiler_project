class CompilerException(Exception):
    def __init__(self, msg, line):
        self.msg = "ERROR (Line " + str(line) + "): " + msg
    
class EOFException(CompilerException):
    pass

class UnexpectedSymbolException(CompilerException):
    pass

class BadTokenException(CompilerException):
    pass

class BadIdentifierException(CompilerException) :
    pass

class BadMemberException(CompilerException) :
    pass

class BadClassException(CompilerException) :
    pass

class UnassignedVarException(CompilerException) :
    pass

class TypeMismatchException(CompilerException) :
    pass

class UnreachableCodeException(CompilerException) :
    pass